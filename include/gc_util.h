#include <gc/gc.h>

#define GC_PLACE(TYPE,VAR,ARGS) \
    TYPE *VAR = (TYPE *)GC_malloc(sizeof(TYPE)); \
    new (VAR) TYPE ARGS
