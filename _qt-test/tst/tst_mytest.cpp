﻿#include <QtTest>
#include "mylib.h"

// add necessary includes here

class mytest : public QObject
{
    Q_OBJECT

public:
    mytest() {}
    ~mytest() {}

private slots:
    //void initTestCase() {}
    //void cleanupTestCase() {}
    void test_case1()
    {
        QCOMPARE(MyLib::add2(11, 22), 33);
    }
};

QTEST_MAIN(mytest)

#include "tst_mytest.moc"
