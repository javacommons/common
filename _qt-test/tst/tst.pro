QT += testlib
QT += gui

CONFIG += qt warn_on depend_includepath testcase
CONFIG += c++17
CONFIG += console
CONFIG += force_debug_info

TEMPLATE = app

include($$(HOME)/common/include/include.pri)
include($$(HOME)/common/common/common.pri)
include($$PWD/../lib/lib.pri)

gcc:QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter -Wno-unused-function -Wno-cast-function-type
msvc:QMAKE_CXXFLAGS += /bigobj

SOURCES +=  tst_mytest.cpp

DESTDIR = $$PWD
