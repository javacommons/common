#lang racket

(require json)

(define in (open-input-file "test.jcon"))
(define jcob (read-json in))
(writeln jcob)
