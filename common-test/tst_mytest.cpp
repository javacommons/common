﻿#include <QtTest>
#include "os.h"

// add necessary includes here

class mytest : public QObject
{
    Q_OBJECT

public:
    mytest() {}
    ~mytest() {}
    QVariant v2v(const QVariant &x)
    {
        return jcon_to_variant(variant_to_jcon(x));
    }

private slots:
    void initTestCase()
    {
        QVariant data = QVariantMap {
        { "a", 1.23 },
        { "b", std::numeric_limits<quint64>::max() },
        { "dt", QDateTime(QDate(2022, 9, 7), QTime(1, 47, 04, 191)) } };

        QString jcon = variant_to_jcon(data, true);
        QFile file("test.jcon");
        if (file.open(QIODevice::WriteOnly))
        {
            file.write(jcon.toUtf8());
            file.close();
        }
    }
    //void cleanupTestCase();
    void test_case1()
    {
        qDebug().noquote() << std::numeric_limits<qint64>::max();
        qDebug().noquote() << std::numeric_limits<qint64>::min();
        qDebug().noquote() << std::numeric_limits<quint64>::max();
        QCOMPARE(variant_to_jcon(std::numeric_limits<quint64>::max()), "{\"!\":\"bigint\",\"?\":\"18446744073709551615\"}");
        QCOMPARE(variant_to_jcon(std::numeric_limits<qint64>::min()), "{\"!\":\"bigint\",\"?\":\"-9223372036854775808\"}");
        QCOMPARE(variant_to_jcon(1.23), "1.23");
        QCOMPARE(variant_to_jcon(1.23, true), "{\"!\":\"double\",\"?\":\"rkfhehSu8z8=\",\"value\":1.23}");
        QCOMPARE(variant_to_jcon(QByteArray("abc")), "{\"!\":\"bytearray\",\"?\":\"YWJj\"}");
        //qDebug().noquote() << v2v(QVariantList() << "a" << 1.23);
    }
    void test_case2()
    {
        QDateTime dt = QDateTime(QDate(2022, 9, 7), QTime(1, 47, 04, 191));
        QCOMPARE(variant_to_jcon(dt), "{\"!\":\"datetime\",\"?\":1662482824191}");
        QCOMPARE(jcon_to_variant("{\"!\":\"datetime\",\"?\":1662482824191}"), dt);
        QVariant v = (QVariantList() << "a" << 1.23 << QDateTime::currentDateTime());
    }
};


QTEST_MAIN(mytest)

#include "tst_mytest.moc"
