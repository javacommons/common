QT += testlib
QT += gui
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

include($$PWD/../include/include.pri)
include($$PWD/../common/common.pri)

SOURCES +=  tst_mytest.cpp

DESTDIR = $$PWD

DISTFILES += \
    test.jcon
