win32 {
  gcc {
    message( "[fltk.pri win32/gcc]" )
    QMAKE_CXXFLAGS += -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64
    LIBS += -lfltk -lole32 -lcomctl32 -lws2_32 -lgdi32 -luuid
  }
  msvc {
  }
}
else {
  message( "[fltk.pri else]" )
  QMAKE_CXXFLAGS += -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_THREAD_SAFE -D_REENTRANT
  LIBS += -lfltk -lX11 -lXext -ldl
}
