﻿#ifndef MYLIB_H
#define MYLIB_H

#include "spiderscript.h"
#include <QtCore>
#include <QtScript>
//#include "VariantToText.h"

class ApplicationData : public QObject, protected QScriptable
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit ApplicationData(QObject *parent=nullptr)
        : QObject(parent)
    {
    }
    virtual ~ApplicationData()
    {
        qDebug() << "~ApplicationData()";
    }
    Q_INVOKABLE QScriptValue throwError(){
        this->context()->throwError("エラーです!");
        return QScriptValue::UndefinedValue;
    }
    Q_INVOKABLE QString getTextFromCpp(){
        qDebug() << "getTextFromCpp() called";
        return QString("This is the text from C++");
    }
    Q_INVOKABLE QScriptValue varTest(const QScriptValue &arg){
        qDebug().noquote() << "varTest().arg=" << scriptValueToText(arg);
        return 777;
    }
    Q_INVOKABLE void exit(int code = 0){
        qDebug() << "exit() called";
        ::exit(code);
    }
    Q_INVOKABLE QString getHomeDir(){
        return QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    }
    Q_INVOKABLE QString getAppDir(){
        return qApp->applicationDirPath();
    }
};

//Q_SCRIPT_DECLARE_QMETAOBJECT(ApplicationData, QObject*)

#if 0x0
class ApplicationFactory : public QObject, protected QScriptable
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit ApplicationFactory(QObject *parent=nullptr)
        : QObject(parent)
    {
    }
    virtual ~ApplicationFactory()
    {
        qDebug() << "~ApplicationFactory()";
    }
    Q_INVOKABLE ApplicationData *newApplicationData(){
        return new class ApplicationData();
    }
};
#endif

#endif // MYLIB_H
