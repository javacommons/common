﻿#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include "spiderscript.h"
#include "utf8LogHandler.h"
#include "debug_line.h"

#include "mylib.h"

QScriptValue add(QScriptContext *ctx, QScriptEngine *engine)
{
    Q_UNUSED(engine);
    if (ctx->argumentCount() != 2)
        return ctx->throwError("add() takes exactly two arguments");
    double a = ctx->argument(0).toNumber();
    double b = ctx->argument(1).toNumber();
    return a + b;
}

Q_SCRIPT_DECLARE_QMETAOBJECT(ApplicationData, QObject*)

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qRegisterMetaType<ApplicationData*>();

    SpiderScript engine;
    QScriptValue objectwin= engine.scriptValueFromQMetaObject<ApplicationData>();
    engine.globalObject().setProperty("ApplicationData", objectwin);

    engine.evaluate("print(11+22)");

    QScriptValue fun = engine.newFunction(add);
    engine.globalObject().setProperty("add", fun);
    QScriptValue result = engine.evaluate("add(22, 33)");
    qDebug() << result.toInteger();

    //ApplicationFactory aFactory;
    //engine.globalObject().setProperty("factory", engine.newQObject(&aFactory, QScriptEngine::QtOwnership));
    ApplicationData aData;
    engine.globalObject().setProperty("appData", engine.newQObject(&aData, QScriptEngine::QtOwnership));

    //engine.evaluateFile("main.js");
    QScriptValue result2 = engine.evaluate(R"***(
var ad = new ApplicationData();
console.log(ad.getHomeDir());
function Car(brand) {
   this.brand = brand;
}
Car.prototype.getBrand = function() {
   return this.brand;
}
var foo = new Car("toyota");
console.log(foo.getBrand());  // "toyota"
var ary = [11, 22, 33];
ary["xyz"] = 44;
ary[9] = 55;
console.log(ary);
function fun(x) { return x + 1; }
var a = new ApplicationData();
console.log(a.getTextFromCpp());
console.log(appData.getTextFromCpp());
console.log(123, 456);
console.log({a: a, b: [fun, 1, 2.34, true, null, undefined, /ab+c/], c: function() {}, d: new Date(), e: "漢字abc\nxyz"});
console.log(["console", "log", /ab+c/i, /\w+/, '"', "\a\b\f\n\r\t\v"]);
var b = a.varTest({a: a, b: [fun, 1, 2.34, true, null, undefined, /ab+c/], c: function() {}, d: new Date(), e: "漢字abc\nxyz"});
console.log(a.getHomeDir());
console.log(a.getHomeDir() + "/.software");
console.log(a.getHomeDir() + "/.software/msys64c");
//xxx();
//appData.exit(0);
    )***");
    if (result2.isError())
    {
        return 1;
    }
    return 0; //app.exec();
}
