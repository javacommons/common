#ifndef NAMEDPIPE_H
#define NAMEDPIPE_H

#include <windows.h>
#include <QtCore>
#include <string>

class NamedPipeServer : QObject
{
    Q_OBJECT
protected:
    QString name_;
    HANDLE hPipe_;
public:
    NamedPipeServer(const QString &name)
        : name_(name)
    {
        hPipe_ = INVALID_HANDLE_VALUE;
        QString pipeName = QString("\\\\.\\pipe\\%1").arg(name);
        hPipe_ = CreateNamedPipeW(pipeName.toStdWString().c_str(), //lpName
                               PIPE_ACCESS_INBOUND,                // dwOpenMode
                               PIPE_TYPE_BYTE | PIPE_WAIT,         // dwPipeMode
                               1,                                  // nMaxInstances
                               0,                                  // nOutBufferSize
                               0,                                  // nInBufferSize
                               100,                                // nDefaultTimeOut
                               NULL);                              // lpSecurityAttributes

        if (hPipe_ == INVALID_HANDLE_VALUE) {
            qDebug() << "Couldn't create NamedPipe.";
            exit(1);
        }
        if (!ConnectNamedPipe(hPipe_, NULL)) {
            qDebug() << "Couldn't connect to NamedPipe.";
            CloseHandle(hPipe_);
            exit(1);
        }
        qDebug() << "Connected to NamedPipe.";
    }
    QString readString()
    {
        QThread::msleep(1);
        char szBuff[256];
        DWORD dwBytesRead;
        if (!ReadFile(hPipe_, szBuff, sizeof(szBuff), &dwBytesRead, NULL)) {
                fprintf(stderr, "Couldn't read NamedPipe.");
                return "";
        }
        szBuff[dwBytesRead] = '\0';
        std::string s = szBuff;
        return QString::fromStdString(s);
    }
    virtual ~NamedPipeServer()
    {
        FlushFileBuffers(hPipe_);
        DisconnectNamedPipe(hPipe_);
        CloseHandle(hPipe_);
    }
};

class NamedPipeClient : QObject
{
    Q_OBJECT
protected:
    QString name_;
    HANDLE hPipe_;
public:
    NamedPipeClient(const QString &name)
        : name_(name)
    {
        hPipe_ = INVALID_HANDLE_VALUE;
        QString pipeName = QString("\\\\.\\pipe\\%1").arg(name);
        hPipe_ = CreateFileW(pipeName.toStdWString().c_str(),
                            GENERIC_WRITE,
                            0,
                            NULL,
                            OPEN_EXISTING,
                            FILE_ATTRIBUTE_NORMAL,
                            NULL);

        if (hPipe_ == INVALID_HANDLE_VALUE) {
            qDebug() << "Couldn't create NamedPipe.";
            exit(1);
        }
        qDebug() << "Connected to NamedPipe.";
    }
    bool writeString(const QString &s)
    {
        DWORD dwBytesWritten;
        QByteArray ba = s.toUtf8();
        if (!WriteFile(hPipe_, ba.data(), ba.size(), &dwBytesWritten, NULL)) {
            qDebug() << "Couldn't write NamedPipe.";
            return false;
        }
        return true;
    }
    virtual ~NamedPipeClient()
    {
        CloseHandle(hPipe_);
    }
};

#endif // NAMEDPIPE_H
