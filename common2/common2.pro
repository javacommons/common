QT += core gui widgets network script scripttools

TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++17
CONFIG += force_debug_info

gcc:QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter -Wno-unused-function

DEFINES += CPPHTTPLIB_OPENSSL_SUPPORT
DEFINES += HTTP_API_USE_QT

INCLUDEPATH += $$PWD
INCLUDEPATH += $$(HOME)/common/include

msvc:INCLUDEPATH += $$(HOME)/conan/binaries/include
msvc:LIBS += -L$$(HOME)/conan/binaries/lib libcrypto.lib libssl.lib
gcc:LIBS += -lssl -lcrypto

DESTDIR += $$PWD

#HEADERS += \
#    spiderscript.h

#SOURCES += \
#    spiderscript.cpp

TARGET = $${TARGET}.qt$${QT_MAJOR_VERSION}-$${QMAKE_HOST.arch}
#message($$QMAKE_QMAKE)
contains(QMAKE_QMAKE, .*static.*) {
    message( "[STATIC BUILD]" )
    DEFINES += QT_STATIC_BUILD
    TARGET = $${TARGET}-static
} else {
    message( "[SHARED BUILD]" )
}
message($$TARGET)
MOC_DIR = build-$${TARGET}
OBJECTS_DIR = build-$${TARGET}
RCC_DIR = build-$${TARGET}
UI_DIR = build-$${TARGET}

HEADERS += \
    namedpipe.h
