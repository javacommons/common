﻿#ifndef SPIDERSCRIPT_H
#define SPIDERSCRIPT_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QtScript>
#include <QtScriptTools>

QString scriptValueToText(const QScriptValue &x);

class SpiderScript : public QScriptEngine
{
    Q_OBJECT
    QScriptEngineDebugger m_debugger;
public:
    explicit SpiderScript(int w = 1024, int h = 640, QObject *parent = nullptr);
    virtual ~SpiderScript();
    QScriptValue evaluateFile(const QString &fileName);
protected:
    static QScriptValue print(QScriptContext *ctx, QScriptEngine *engine);
};

#endif // SPIDERSCRIPT_H
