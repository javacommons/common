﻿#include "jcon.h"

#define MAX_SAFE_INTEGER 9007199254740991LL
#define MIN_SAFE_INTEGER -9007199254740991LL

nlohmann::json variant_to_jcob(const QVariant &x, bool encode_double)
{
    QString typeName = x.typeName();
    nlohmann::json result = typeName.toStdString();
    if (!x.isValid() || x.isNull())
    {
        result = nullptr;
    }
    else if (typeName == "QVariantMap")
    {
        result = nlohmann::json::object();
        QVariantMap map = x.toMap();
        QStringList keys = map.keys();
        foreach(QString key, keys)
        {
            result[key.toStdString()] = variant_to_jcob(map[key], encode_double);
        }
        if (result.contains("!"))
        {
            nlohmann::json parent = nlohmann::json::object();
            parent["!"] = "dictionary";
            parent["?"] = result;
            result = parent;
        }
    }
    else if (typeName == "QVariantList")
    {
        result = nlohmann::json::array();
        QVariantList list = x.toList();
        for(int i=0; i<list.size(); i++)
        {
            result[i] = variant_to_jcob(list[i], encode_double);
        }
    }
    else if (typeName == "float"||
             typeName == "double")
    {
        //qDebug() << "float||double";
        double value = x.toDouble();
        result = value;
        if (encode_double)
        {
            //qDebug() << "encode_double";
            result = nlohmann::json::object();
            result["!"] = "double";
            result["?"] = QByteArray((char *)&value, 8).toBase64().toStdString();
            result["value"] = value;
        }
    }
    else if (typeName == "int" ||
             typeName == "qlonglong")
    {
        qlonglong value = x.toLongLong();
        result = value;
        if (value > MAX_SAFE_INTEGER)
        {
            result = nlohmann::json::object();
            result["!"] = "bigint";
            result["?"] = x.toString().toStdString();
        }
        else if (value < MIN_SAFE_INTEGER)
        {
            result = nlohmann::json::object();
            result["!"] = "bigint";
            result["?"] = x.toString().toStdString();
        }
    }
    else if (typeName == "uint" ||
             typeName == "qulonglong")
    {
        qulonglong value = x.toULongLong();
        result = value;
        if (value > MAX_SAFE_INTEGER)
        {
            result = nlohmann::json::object();
            result["!"] = "bigint";
            result["?"] = x.toString().toStdString();
        }
    }
    else if (typeName == "bool")
    {
        result = x.toBool();
    }
    else if (typeName == "QString")
    {
        result = x.toString().toUtf8().toStdString();
    }
    else if (typeName == "QByteArray")
    {
        std::string s = x.toByteArray().toBase64().toStdString();
        result = nlohmann::json::object();
        result["!"] = "bytearray";
        result["?"] = s;
    }
    else if (typeName == "QDateTime")
    {
        QDateTime dt = x.toDateTime();
        result = nlohmann::json::object();
        result["!"] = "datetime";
        result["?"] = variant_to_jcob(dt.toMSecsSinceEpoch());
    }
    else
    {
        return typeName.toStdString();
    }
    return result;
}

QVariant jcob_to_variant(const nlohmann::json &x)
{
    QString type;
    switch (x.type())
    {
    case nlohmann::json::value_t::null: ///< null value
    {
        type = "null";
        return QVariant();
    }
        break;
    case nlohmann::json::value_t::object: ///< object (unordered set of name/value pairs)
    {
        type = "object";
        QVariantMap result;
        for (auto &el : x.items())
        {
            result[QString::fromStdString(el.key())] = jcob_to_variant(el.value());
        }
        if (result.contains("!"))
        {
            if (result["!"].toString()=="dictionary")
            {
                return result["?"];
            }
            else if (result["!"].toString()=="bigint")
            {
                QString n = result["?"].toString();
                if (n.startsWith("-"))
                {
                    return result["?"].toLongLong();
                }
                return result["?"].toULongLong();
            }
            else if (result["!"].toString()=="bytearray")
            {
                return QByteArray::fromBase64(result["?"].toByteArray());
            }
            else if (result["!"].toString()=="datetime")
            {
                QDateTime dt;
                dt.setMSecsSinceEpoch(result["?"].toLongLong());
                return dt;
            }
            else if (result["!"].toString()=="double")
            {
                QByteArray bytes = QByteArray::fromBase64(result["?"].toByteArray());
                double n = 0.0;
                if (bytes.size()!=8) return QVariant::fromValue(n);
                n = *((double *)bytes.constData());
                return QVariant::fromValue(n);
            }
        }
        return result;
    }
        break;
    case nlohmann::json::value_t::array: ///< array (ordered collection of values)
    {
        type = "array";
        QVariantList result;
        for (auto &el : x)
        {
            result.append(jcob_to_variant(el));
        }
        return result;
    }
        break;
    case nlohmann::json::value_t::string: ///< string value
    {
        type = "string";
        return QString::fromStdString(x.get<std::string>());
    }
        break;
    case nlohmann::json::value_t::boolean: ///< boolean value
    {
        type = "boolean";
        return x.get<bool>();
    }
        break;
    case nlohmann::json::value_t::number_integer: ///< number value (signed integer)
    {
        type = "integer";
        return x.get<std::int64_t>();
    }
        break;
    case nlohmann::json::value_t::number_unsigned: ///< number value (unsigned integer)
    {
        type = "unsigned";
        return x.get<std::uint64_t>();
    }
        break;
    case nlohmann::json::value_t::number_float: ///< number value (floating-point)
    {
        type = "float";
        return x.get<double>();
    }
        break;
    case nlohmann::json::value_t::binary: ///< binary array (ordered collection of bytes)
    {
        type = "binary";
        const nlohmann::json::binary_t &bin = x.get_binary();
        std::string s(bin.begin(), bin.end());
        QByteArray bytes = QByteArray::fromStdString(s);
        if (bin.subtype() == 1)
        {
            QVariant v;
            QDataStream in(&bytes, QIODevice::ReadOnly);
            in.setVersion(QDataStream::Qt_5_1);
            in >> v;
            return v;
        }
        return bytes;
    }
        break;
    case nlohmann::json::value_t::discarded: ///< discarded by the parser callback function
    {
        type = "discarded";
    }
        break;
    default:
    {
        type = "?";
    }
        break;
    }
    return type;
}

std::string variant_to_jcon_ss(const QVariant &x, bool encode_double)
{
    return variant_to_jcob(x, encode_double).dump(2);
}

QVariant jcon_ss_to_variant(const std::string &x)
{
    nlohmann::json jcob = nlohmann::json::parse(x);
    return jcob_to_variant(jcob);
}

QString variant_to_jcon(const QVariant &x, bool encode_double)
{
    return QString::fromStdString(variant_to_jcon_ss(x, encode_double));
}

QVariant jcon_to_variant(const QString &x)
{
    return jcon_ss_to_variant(x.toStdString());
}
