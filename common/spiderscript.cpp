﻿#include "spiderscript.h"

static inline QString escapeChar(const QChar &c)
{
    // https://en.cppreference.com/w/cpp/language/escape
    wchar_t wc = c.unicode();
    switch(wc)
    {
    case L'"':
        return "\\\"";
    case L'\\':
        return "\\\\";
    case L'\a':
        return "\\a";
    case L'\b':
        return "\\b";
    case L'\f':
        return "\\f";
    case L'\n':
        return "\\n";
    case L'\r':
        return "\\r";
    case L'\t':
        return "\\t";
    case L'\v':
        return "\\v";
    default:
        return QString(c);
        break;
    }
}

static inline QString escapeString(const QString &s)
{
    QString result = "\"";
    for(int i=0; i<s.size(); i++)
    {
        result += escapeChar(s[i]);
    }
    result += "\"";
    return result;
}

QString scriptValueToText(const QScriptValue &x)
{
    if(x.isVariant())
    {
        //return variantToText(x.toVariant());
        return x.toVariant().toString();
    }
    else if(x.isQObject())
    {
        QObject *qobj = x.toQObject();
        return QString("QObject(%1)").arg(qobj->metaObject()->className());
    }
    else if(x.isRegExp())
    {
#if 0x0
        QRegExp rx = x.toRegExp();
        if(rx.caseSensitivity() == Qt::CaseSensitive)
        {
            return QString("RegExp(%1)").arg(escapeString(rx.pattern()));
        }
        return QString("RegExp(%1, \"i\")").arg(escapeString(rx.pattern()));
#else
        return x.toString();
#endif
    }
    else if(x.isFunction())
    {
        QString name = x.property("name").toString();
        if(name.isEmpty()) return "<anonymous function>";
        return QString("<function %1>").arg(name);
    }
    else if(x.isDate())
    {
        return QString("Date(%1)").arg(x.toDateTime().toString("yyyy-MM-ddThh:mm:ss.z"));
    }
    else if(x.isArray())
    {
        const QScriptValue &array = x;
        QString result = "[";
        qint32 length = array.property("length").toInt32();
        qint32 i;
        for(i = 0; i < length; i++)
        {
            if(i>0) result += ", ";
            result += scriptValueToText(array.property(i));
        }
        QScriptValueIterator it(array);
        while(it.hasNext()) {
            it.next();
            if( it.flags() & QScriptValue::SkipInEnumeration )
                continue;
            QString name = it.name();
            bool ok;
            name.toInt(&ok);
            if(ok) continue;
            if(i>0) result += ", ";
            result += scriptValueToText(name);
            result += ": ";
            result += scriptValueToText(it.value());
            i++;
        }
        result += "]";
        return result;
    }
    else if(x.isObject())
    {
        const QScriptValue &object = x;
        QScriptValueIterator it(object);
        QString result = "{";
        qint64 i = 0;
        while(it.hasNext())
        {
            it.next();
            if( it.flags() & QScriptValue::SkipInEnumeration )
                continue;
            if(i>0) result += ", ";
            result += scriptValueToText(it.name());
            result += ": ";
            result += scriptValueToText(it.value());
            i++;
        }
        result += "}";
        return result;
    }
    else if(x.isString())
    {
        return escapeString(x.toString());
    }
    else if(x.isValid())
    {
        return x.toString();
    }
    else
    {
        return "#";
    }
}

SpiderScript::SpiderScript(int w, int h, QObject *parent) : QScriptEngine(parent)
{
    this->globalObject().setProperty("print", this->newFunction(print));
    QScriptValue console = this->newObject();
    console.setProperty("log", this->newFunction(print));
    this->globalObject().setProperty("console", console);
    m_debugger.attachTo(this);
    QMainWindow *debugWindow = m_debugger.standardWindow();
    debugWindow->resize(w, h);
}

SpiderScript::~SpiderScript()
{
}

QScriptValue SpiderScript::evaluateFile(const QString &fileName)
{
    QFile file(fileName);
    file.open(QIODevice::ReadOnly);
    return this->evaluate(file.readAll(), fileName);
}

QScriptValue SpiderScript::print(QScriptContext *ctx, QScriptEngine *engine)
{
    Q_UNUSED(engine);
    QString msg;
    for(int i=0; i<ctx->argumentCount(); i++)
    {
        if(i>0) msg += " ";
        QScriptValue val = ctx->argument(i);
        msg += scriptValueToText(ctx->argument(i));
    }
    qDebug().noquote() << msg;
    return QScriptValue::UndefinedValue;
}
