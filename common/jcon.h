﻿#ifndef JCON_H
#define JCON_H

#include <QtCore>
//#include <iostream>
#include <nlohmann/json.hpp>

nlohmann::json variant_to_jcob(const QVariant &x, bool encode_double = false);

QVariant jcob_to_variant(const nlohmann::json &x);

std::string variant_to_jcon_ss(const QVariant &x, bool encode_double = false);

QVariant jcon_ss_to_variant(const std::string &x);

QString variant_to_jcon(const QVariant &x, bool encode_double = false);

QVariant jcon_to_variant(const QString &x);

#endif // JCON_H
