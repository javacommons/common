﻿#include "variantserializer.h"
#include "jcon.h"

//#define USE_CBOR_AS_BINARY

static QString variantToString(const QVariant &v)
{
    QJsonDocument jdoc = QJsonDocument::fromVariant(v);
    QString json = QString::fromUtf8(jdoc.toJson(QJsonDocument::Compact));
    if (!json.isEmpty())
    {
        return json;
    }
    QVariantList vl = {v};
    jdoc = QJsonDocument::fromVariant(vl);
    json = QString::fromUtf8(jdoc.toJson(QJsonDocument::Compact));
    json = json.mid(1, json.length() - 2); // remove [ & ]
    return json;
}
VariantSerializer::VariantSerializer()
{
}
QByteArray VariantSerializer::serialize(const QVariant &x)
{
    return this->serializeToBinary(x).toBase64();
}
QVariant VariantSerializer::deserialize(const QByteArray &x)
{
    return this->deserializeFromBinary(QByteArray::fromBase64(x));
}
QString VariantSerializer::serializeToString(const QVariant &x)
{
    return variant_to_jcon(x, true);
}
QVariant VariantSerializer::deserializeFromString(const QString &x)
{
    return jcon_to_variant(x);
}
QByteArray VariantSerializer::serializeToBinary(const QVariant &x)
{
#ifdef USE_CBOR_AS_BINARY
    QCborValue cbv = QCborValue::fromVariant(x);
    return cbv.toCbor();
#else
    QByteArray message;
    QDataStream out(&message, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_1);
    out << x;
    return message;
#endif
}
QVariant VariantSerializer::deserializeFromBinary(const QByteArray &x)
{
#ifdef USE_CBOR_AS_BINARY
    QCborValue cbv = QCborValue::fromCbor(x);
    return cbv.toVariant();
#else
    QByteArray message = x;
    QVariant v;
    QDataStream in(&message, QIODevice::ReadOnly);
    in.setVersion(QDataStream::Qt_5_1);
    in >> v;
    return v;
#endif
}

std::string VariantSerializer::serializeToStdString(const QVariant &x)
{
    return variant_to_jcon_ss(x, true);
}

QVariant VariantSerializer::deserializeFromStdString(const std::string &x)
{
    return jcon_ss_to_variant(x);
}
