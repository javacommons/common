﻿#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QtQml>
#include "utf8LogHandler.h"
#include "debug_line.h"

#include "mylib.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qInstallMessageHandler(utf8LogHandler);
    qRegisterMetaType<MyObject *>();
    QJSEngine engine;
    engine.installExtensions(QJSEngine::AllExtensions);
    Root root(&engine);
    MyObject obj(&engine);
    engine.globalObject().setProperty("root", engine.newQObject(&root));
    engine.globalObject().setProperty("myObject", engine.newQObject(&obj));
    qDebug() << "Hello World!";
    qDebug() << engine.evaluate("11+22").toNumber();
    qDebug() << engine.evaluate("myObject.add2(22, 33)").toString();

    QJSValue result  = engine.evaluate(R"(var o = root.createMyObject();o.add2(44, 55))");
    qDebug() << result.toNumber();
    result  = engine.evaluate(R"(var ox = root.createMyObject(); ox.add2(44, 55))", "script", 1);
    if(result.isError())
    {
        qDebug().noquote()
              << "(1)Uncaught exception at"
              << result.property("fileName").toString()
              << "line" << result.property("lineNumber").toInt()
              << ":" << result.toString();
    }
    result  = engine.evaluate(R"(root.createMyObject().errorTest())", "script", 1);
    if(result.isError())
    {
        qDebug().noquote()
              << "(2)Uncaught exception at"
              << result.property("fileName").toString()
              << "line" << result.property("lineNumber").toInt()
              << ":" << result.toString();
    }
    result  = engine.evaluate(R"(var oy = root.createMyObject(); oy.errorTest())", "script", 1);
    if(result.isError())
    {
        qDebug().noquote()
              << "(3)Uncaught exception at"
              << result.property("fileName").toString()
              << "line" << result.property("lineNumber").toInt()
              << ":" << result.toString();
    }
    result = engine.importModule("./main.mjs");
    if(!result.isError())
    {
        result.property("func").call();
    }
    else
    {
        qDebug().noquote()
              << "(4)Uncaught exception at"
              << result.property("fileName").toString()
              << "line" << result.property("lineNumber").toInt()
              << ":" << result.toString();
    }
    return 0;
}
