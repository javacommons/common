﻿#ifndef MYLIB_H
#define MYLIB_H

#include <QtCore>
#include <QtQml>

class MyObject: public QObject
{
    Q_OBJECT
private:
    QJSEngine *m_engine = nullptr;
public:
    Q_INVOKABLE explicit MyObject(QJSEngine *engine, QObject *parent = nullptr)
        : QObject(parent), m_engine(engine)
    {
    }
public slots:
    double add2(double x, double y)
    {
        return x + y;
    }
    QJSValue errorTest()
    {
        m_engine->throwError(QString("error test"));
        return QJSValue();
    }
};

class Root: public QObject
{
    Q_OBJECT
private:
    QJSEngine *m_engine = nullptr;
public:
    Q_INVOKABLE explicit Root(QJSEngine *engine, QObject *parent = nullptr)
        : QObject(parent), m_engine(engine)
    {
    }
    Q_INVOKABLE MyObject *createMyObject()
    {
        return new MyObject(m_engine);
    }
};

#endif // MYLIB_H
